import { IsEmpty } from "class-validator"
import { MinLength } from "class-validator/types/decorator/decorators"

export class CreateCustomerDto {

  @IsEmpty()
  name: string

  @IsEmpty()
  age: number

  @IsEmpty()
  @MinLength(10)
  tal: string

  @IsEmpty()
  gender: string

}
