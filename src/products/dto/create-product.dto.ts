import { IsEmpty } from "class-validator"
import { IsPositive } from "class-validator/types/decorator/decorators"

export class CreateProductDto {
  @IsEmpty()
  name: string

  @IsEmpty()
  @IsPositive()
  price: number

}
