import { IsEmpty } from "class-validator";

class CreateOrderItemDto {
  @IsEmpty()
  productId: number;
  @IsEmpty()
  amount: number;
}


export class CreateOrderDto {

  @IsEmpty()
  customerId: number;

  @IsEmpty()
  orderItems: CreateOrderItemDto[];
}
