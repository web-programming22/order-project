import { Product } from "src/products/entities/product.entity";
import { Column, CreateDateColumn, DeleteDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { Order } from "./order.entity";


@Entity()
export class OrderItem {
  @PrimaryGeneratedColumn()
  id: number

  @Column()
  name: string

  @Column({ type: 'float' })
  price: number

  @Column()
  amount: number

  @Column({ type: 'float' })
  total: number

  @ManyToOne(()=>Product, (product)=>product.orderItem)
  product: Product

  @ManyToOne(() => Order, (order) => order.orderItem)
  order: Order

  @CreateDateColumn()
  createdDate: Date

  @UpdateDateColumn()
  updatedDate: Date

  @DeleteDateColumn()
  delatedDate: Date
}