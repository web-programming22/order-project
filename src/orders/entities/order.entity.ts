import { Customer } from "src/customers/entities/customer.entity";
import { Column, CreateDateColumn, DeleteDateColumn, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { OrderItem } from "./order_item";


@Entity()
export class Order {
  @PrimaryGeneratedColumn()
  id: number

  @CreateDateColumn()
  createdDate: Date

  @UpdateDateColumn()
  updatedDate: Date

  @DeleteDateColumn()
  delatedDate: Date

  @Column({ type: 'float' })
  total: number

  @Column()
  amount: number

  @ManyToOne(() => Customer, (customer) => customer.orders)
  customer: Customer


  @OneToMany(() => OrderItem, (orderItem) => orderItem.order)
  orderItem: OrderItem[]

}
